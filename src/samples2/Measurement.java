package samples2;

import java.time.*;

import static java.util.Objects.requireNonNull;

public class Measurement {

	public enum Result {
		NEGATIVE, DOUBTFUL, POSITIVE;
	}

	private final LocalDateTime measuredDateTime;

	private final double value;

	private final Result result;

	public Measurement(LocalDateTime measuredDateTime, double value, Result result) {
		this.measuredDateTime = requireNonNull(measuredDateTime, "Must be a Value");
		this.value = SampleResult.validInvestigationValue(value);
		this.result = requireNonNull(result, "Must be a Value");
	}

	@Override
	public String toString() {
		return "Measurement [measuredDateTime=" + measuredDateTime + ", value=" + value + ", result=" + result + "]";
	}

	public LocalDateTime getMeasuredDateTime() {
		return measuredDateTime;
	}

	public double getValue() {
		return value;
	}

	public Result getResult() {
		return result;
	}

}
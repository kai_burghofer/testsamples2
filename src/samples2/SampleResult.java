package samples2;

import samples2.Measurement.Result;

public class SampleResult {

	private static final double THRESHOLD_NEGATIVE_VALUE = 5.5;

	private static final double THRESHOLD_POSITIVE_VALUE = 21.0;

	static double validInvestigationValue(double investigationValue) {

		if (!Double.isFinite(investigationValue))
			throw new IllegalArgumentException("Invalid investigation value: " + investigationValue);

		return investigationValue;
	}

	static Result calculateResult(double investigationValue) {

		final double validValue = validInvestigationValue(investigationValue);

		if (validValue < THRESHOLD_NEGATIVE_VALUE)
			return Result.NEGATIVE;

		else if (validValue <= THRESHOLD_POSITIVE_VALUE)
			return Result.DOUBTFUL;

		else
			return Result.POSITIVE;
	}

}

package samples2;

import java.time.*;

import static java.util.Objects.requireNonNull;

public final class Sample {

	private final String id;

	private final LocalDate creationDate;

	private final String discription;

	private Measurement measurement;

	public Sample(String id) {
		this(id, null, null);
	}

	public Sample(String id, LocalDate creationDate) {
		this(id, creationDate, null);
	}

	public Sample(String id, LocalDate creationDate, String discription) {
		this.id = requireNonNull(id, "it must be a Value");
		this.creationDate = requireNonNull(creationDate, "it must be a Value");
		this.discription = discription;
	}

	@Override
	public String toString() {
		return "Samples [id=" + id + ", creationDate=" + creationDate + ", discription=" + discription + ", measurement=" + measurement + "]";
	}

	public String getDiscription() {
		return discription;
	}

	public String getId() {
		return id;
	}

	public LocalDate getCreationDate() {
		return creationDate;
	}

	public Measurement getMeasurement() {
		return measurement;
	}

	public void setMeasurement(Measurement measurement) {
		this.measurement = measurement;
	}

}

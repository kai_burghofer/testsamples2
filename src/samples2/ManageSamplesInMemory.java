package samples2;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

public class ManageSamplesInMemory implements ManageSamples {

	private List<Sample> samples = new ArrayList<>();

	@Override
	public Sample createSample(String id, LocalDate creationDate, String discription) {
		var sample = new Sample(id, creationDate, discription);

		samples.add(sample);

		return sample;
	}

	@Override
	public void deleteSample(String id) {
		this.samples.removeIf(s -> s.getId().equals(id));
	}

	@Override
	public Sample addMeasuredValue(String id, double value) {
		var sample = getSampleById(id).orElseThrow(() -> new NoSuchElementException("Probe nicht gefunden: " + id));
		sample.setMeasurement(new Measurement(LocalDateTime.now(), value, SampleResult.calculateResult(value)));
		return sample;
	}

	@Override
	public Optional<Sample> getSampleById(String id) {

//		for (var sample : this.samples) {
//			if (sample.getId().equals(id)) return sample;
//		}

		return this.samples.stream() //
				.filter(s -> s.getId().equals(id)) //
				.findFirst();
	}

	@Override
	public List<Sample> listByProperty(Filter filter) {

		return this.samples.stream() //
				.filter(s -> filterByValue(s, filter.value)) //
				.filter(s -> {
					if (filter.measuredDateTime != null && (s.getMeasurement() != null))
						return s.getMeasurement().getMeasuredDateTime().equals(filter.measuredDateTime);
					return true;
				}).filter(s -> {
					if (filter.result != null && (s.getMeasurement() != null))
						return s.getMeasurement().getResult() == filter.result;
					return true;
				}).collect(Collectors.toList());
	}

	private static boolean filterByValue(Sample sample, double value) {
		if (Double.isFinite(value) && (sample.getMeasurement() != null))
			return (sample.getMeasurement().getValue() == value);
		return true;
	}
}

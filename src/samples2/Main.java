
package samples2;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.NoSuchElementException;

public class Main {

	public static void main(String[] args) {

		final var samples = new Sample("2020-004", LocalDate.now(), "");
		System.out.println(samples);

		final var invastigationValue = 4.0;
		final var measurement = new Measurement(LocalDateTime.now(), invastigationValue,
				SampleResult.calculateResult(invastigationValue));

		System.out.println(measurement);

		ManageSamples manager = new ManageSamplesInMemory();
		manager.createSample("42", LocalDate.now(), "Probe");
		manager.createSample("43", LocalDate.now(), "Noch eine Probe");

		var optSample = manager.getSampleById("42");
		System.out.println(optSample.get());

		try {
			var sample = manager.addMeasuredValue("43", 0.43);
			System.out.println(sample);
		} catch (NoSuchElementException ex) {
			System.out.println("Ich konnte die Probe leider nicht finden.");
		}

		var filter = new Filter();
		//filter.value = 0.43;
		var list = manager.listByProperty(filter);
		System.out.println(list);
	}

}

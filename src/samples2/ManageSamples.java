package samples2;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface ManageSamples {

	/**
	 * Legt eine neue Probe mit den angegebenen Parametern an
	 * 
	 * @param id
	 * @param creationDate
	 * @param discription
	 * @return gibt alle angegebenen Werte zurück
	 */

	Sample createSample(String id, LocalDate creationDate, String discription);

	/**
	 * Löscht die Probe mit allen angegebenen Werten
	 * 
	 * @param id
	 * @return gibt {@code null} zurück
	 */

	void deleteSample(String id);

	/**
	 * Fügt einen Messwert hinzu
	 * 
	 * @param value
	 * @return gibt den Messwert zurück
	 */

	Sample addMeasuredValue(String id,double value);

	/**
	 * Sucht und liefert eine Probe über ihre ID.
	 * 
	 * @param id die ID der gesuchten Probe
	 * @return die gefundene Probe
	 */
	
	Optional<Sample> getSampleById(String id);

	/**
	 * Listet alle ausgewerteten Proben sortiert nach den aufgeführten Parametern
	 * auf
	 * 
	 * @param measuredDateTime
	 * @param value
	 * @param result
	 * @return gibt die Werte zurück
	 */

	List<Sample> listByProperty(Filter filter);

}

// var filter = new Filter();
// filter.result = Result.POSITIVE;
// manager.listByProperty(filter);

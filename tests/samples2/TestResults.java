package samples2;

public class TestResults {

	public static double[] invalidDoubles() {
		return new double[] { Double.NaN, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY };
	}

	private TestResults() {
	}

}

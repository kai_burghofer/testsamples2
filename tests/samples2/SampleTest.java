package samples2;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.*;

import org.junit.jupiter.api.Test;

public final class SampleTest {

	private static final String TEST_ID = "2303-05";

	private static final LocalDate TEST_CREATION_DATE = LocalDate.of(2020, 5, 19);

	private static final String TEST_DISCRIPTION = "Test";

	@Test
	public void create() {
		final var samples = new Sample(TEST_ID, TEST_CREATION_DATE, TEST_DISCRIPTION);
		assertEquals(TEST_ID, samples.getId());
		assertEquals(TEST_CREATION_DATE, samples.getCreationDate());
		assertEquals(TEST_DISCRIPTION, samples.getDiscription());
	}

	@Test
	public void createMissingRequired() {
		assertEquals("it must be a Value",
				assertThrows(NullPointerException.class, () -> new Sample(null, TEST_CREATION_DATE, TEST_DISCRIPTION))
						.getMessage());
		assertEquals("it must be a Value",
				assertThrows(NullPointerException.class, () -> new Sample(TEST_ID, null, TEST_DISCRIPTION))
						.getMessage());
	}

}

package samples2;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.*;

import org.junit.jupiter.api.Test;

import samples2.Measurement.Result;


public class MeasurementTest {


	private static final LocalDateTime TEST_MEASURED_DATE_TIME = LocalDateTime.of(2020, 5, 19, 8, 33, 40);

	private static final double TEST_VALUE = 14.7;

	private static final Result TEST_RESULT = Result.DOUBTFUL;

	@Test
	public void create() {
		final var measurement = new Measurement(TEST_MEASURED_DATE_TIME, TEST_VALUE, TEST_RESULT);
		assertEquals(TEST_MEASURED_DATE_TIME, measurement.getMeasuredDateTime());
		assertEquals(TEST_VALUE,measurement.getValue());
		assertEquals(TEST_RESULT,measurement.getResult());
	}
	
	@Test
	public void createMissingRequired() {
		assertEquals("Must be a Value",
				assertThrows(NullPointerException.class, () -> new Measurement(null, TEST_VALUE, TEST_RESULT)).getMessage());
		assertEquals("Must be a Value",
				assertThrows(NullPointerException.class, () -> new Measurement(TEST_MEASURED_DATE_TIME, TEST_VALUE, null)).getMessage());
	}
	
}
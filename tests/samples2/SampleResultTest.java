package samples2;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import samples2.Measurement.Result;

public class SampleResultTest {

@ParameterizedTest
@ValueSource(doubles = {-14.5, -2.7,-0.1, 0.0, 7.2, 18.5, 32.9})
public void validInvestigationValue(double investigationValue) {
	assertEquals(investigationValue, SampleResult.validInvestigationValue(investigationValue));
}

@ParameterizedTest
@MethodSource("samples2.TestResults#invalidDoubles")
public void invalidInvestigationValue(double investigationValue) {
	final var MESSAGE = "Invalid investigation value: " + investigationValue;
	assertEquals(MESSAGE,assertThrows(IllegalArgumentException.class, () -> SampleResult.validInvestigationValue(investigationValue)).getMessage());
}

@ParameterizedTest
@ValueSource(doubles = { -8.4, -2.0, 0.0, 3.0, 5.499})
public void negativeResult(double investigationValue) {
	assertEquals(Result.NEGATIVE, SampleResult.calculateResult(investigationValue));
}

@ParameterizedTest
@ValueSource(doubles = { 8.4, 12.4, 21.0})
public void doubtfulResult(double investigationValue) {
	assertEquals(Result.DOUBTFUL, SampleResult.calculateResult(investigationValue));
}

@ParameterizedTest
@ValueSource(doubles = { 21.001, 33.9, 88.8})
public void positiveResult(double investigationValue) {
	assertEquals(Result.POSITIVE, SampleResult.calculateResult(investigationValue));
}

@ParameterizedTest
@MethodSource("samples2.TestResults#invalidDoubles")
public void invalidResult(double investigationValue) {
	assertThrows(IllegalArgumentException.class, () -> SampleResult.calculateResult(investigationValue));
}
}
